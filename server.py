import web
from web import form
import crypto, hashlib, os

STR_COOKIE_NAME = "auth_token"

master_key = os.urandom(16)

#"Database" of users. Key is username, value is [SHA1(password), userid, role]
user_db = {"admin":["55dce931c04020878dbaa7e242d8819875e2090e", 0, "admin"]}
user_ids = int(1)

render = web.template.render('templates/')
urls = ('/', 'index', 
		'/register', 'register',
		'/logout', 'logout',
		'/home', 'home')

class index:
	login_form = form.Form(
		form.Textbox("user",
			form.notnull,
			description="Username",
			id='usernameBox'),
		form.Password("password",
			form.notnull,
			description="Password",
			id='passwordBox'),
		form.Button("Login",
			id='loginButton'))

	nullform = form.Form()

	def GET(self):
		user, uid, role, valid = verify_cookie()
		if user != "" and valid:
			return render.login(self.nullform, user, "Already logged in.")

		return render.login(self.login_form(), "", "")

	def POST(self):
		form = self.login_form()

		if not form.validates():
			return render.login(form, "", "Invalid form data.")

		user = form.d.user
		pw = hashlib.sha1(form.d.password).hexdigest()

		if user in user_db and user_db[user][0] == pw:
			create_cookie(user, user_db[user][1], user_db[user][2])
			raise web.seeother('/home')
		
		return render.login(form, "", "Username/Password Incorrect")


class register:
	myform = form.Form(
		form.Textbox("user",
			form.notnull,
			description = "Username"),
		form.Password("password",
			form.notnull,
			description = "Password"),
		form.Button("Register",
			description="Register"))

	nullform = form.Form()

	def GET(self):
		user, uid, role, valid = verify_cookie()
		if user != "" and valid:
			return render.generic(self.nullform, user, "", "Already logged in.")

		return render.generic(self.myform(), "", "Enter a username and password.", "")
		

	def POST(self):
		global user_ids
		form = self.myform()
		msg = ""
		err = ""
		
		if not form.validates():
			err = "Invalid fields."
		else:
			if form.d.user in user_db:
				err = "User already registered."
			else:
				#Set the password and role: only non-admin "user" roles can be created
				#through the web interface
				user_db[form.d.user] = [hashlib.sha1(form.d.password).hexdigest(), user_ids, "user"]
				user_ids += 1
				msg = "User registered."
		return render.generic(self.nullform(), "", msg, err)

class logout:
	def GET(self):
		destroy_cookie()
		raise web.seeother('/')

class home:

	def GET(self):
		user, uid, role, valid = verify_cookie()
		
		#Don't really need to check UID, if user and role checks out.
		if user == "" and valid:
			print '403 Forbidden'
			web.ctx.status = '403 Forbidden'
			return render.home("", "", "", "Please log in.")
		elif user == "" and not valid:
			print '498 Invalid Token'
			web.ctx.status = '498 Invalid Token'
			return render.home("", "", "", "Please log in.")
		elif role == "admin":
			msg = "Welcome, Admin!"
		else:
			msg = "Welcome, " + user

		return render.home(user, role, msg, "")

def destroy_cookie():
	web.setcookie(STR_COOKIE_NAME, "", expires=-1)
	
def create_cookie(user, uid, role):
	cookie = crypto.create_crypto_cookie(user, uid, role, master_key)
	web.setcookie(STR_COOKIE_NAME, cookie.encode("hex"))

def verify_cookie():
	cookie = web.cookies().get(STR_COOKIE_NAME)
	if cookie == None:
		return "","","",True
	# Add two exceptions, one for padding error, one for whatever
	# the return in verify_crypto_cookie will return if keys are missing.
	# Maybe add a fourth return value here, False is padding error, True is key error
	try:
		user, uid, role = crypto.verify_crypto_cookie(cookie.decode("hex"), master_key)
		return user,uid,role,True
	except KeyError: #The padding was right, but the cookie failed to decrypt properly
		return "","","",False
	except: #PaddingError: The cookie's padding was wrong
		return "","","",True

if __name__ == "__main__":
	app = web.application(urls, globals())
	app.run()
