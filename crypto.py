from Crypto.Cipher import AES
import urllib, urlparse, os

#Add PKCS7 padding to plaintext 'plain' with block size 'blocksize' 
def pkcs7_pad(plain,blocksize):
    
    #Compute how many pad bytes we need
	padbyte = blocksize - len(plain)%blocksize
    #Add the pad byte and reutrn 
	plain += chr(padbyte) * padbyte
	return plain

#Strip PKCS7 padding from 'padplain' with block size 'blocksize'
def pkcs7_strip(padplain,blocksize):

    #Compute the number of blocks in the string to be padded
	numblocks = len(padplain)/(blocksize) + (1 if len(padplain)%blocksize else 0)

    #Grab all non-padded plaintext
	newplain = padplain[0:(numblocks-1)*blocksize]
    #Grab the block with padding
	padblock = padplain[(numblocks-1)*blocksize:]
    #Compute how many padding bytes do we need to remove
	padbytes = int(padblock[-1:].encode("hex"),16)

	#Validate padding - we should never see a pad end in zero
	if padbytes == 0 or padbytes > blocksize:
		raise Exception("PaddingError")
		return ""
	#make sure all the pad bytes make sense
	if padblock[blocksize-padbytes:blocksize] != chr(padbytes)*padbytes:
		raise Exception("PaddingError")
		return ""
    #If we're the padding is valid, strip it, and append the plaintext
	newplain += padblock[:-padbytes]

	return newplain


def create_crypto_cookie(user, userid, role, key):
	
	#Use quote_plus to catch those l33t h4x0rz 
    #trying to set usernames to "user&role=admin"
	cookie = "user=" + urllib.quote_plus(user) + "&uid=" + str(userid) + "&role=" + role
	#Create a random IV for CBC mode
	iv = os.urandom(AES.block_size)
	aes_obj = AES.new(bytes(key), AES.MODE_CBC, iv)
	return iv + aes_obj.encrypt(pkcs7_pad(cookie, AES.block_size))

def verify_crypto_cookie(enc_cookie, key):
	
	iv = enc_cookie[:AES.block_size]
	aes_obj = AES.new(bytes(key), AES.MODE_CBC, iv)
	cookie_pad = aes_obj.decrypt(enc_cookie[AES.block_size:])
	cookie = pkcs7_strip(cookie_pad, AES.block_size)
	query = urlparse.parse_qs(cookie)
	
	#This will cause an exception (to be caught by caller) if one of the keys is missing.
	return query["user"][0], query["uid"][0], query["role"][0]